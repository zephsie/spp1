﻿using Newtonsoft.Json;
using TracerLib.Models;

namespace TracerLib.Serialization;

public class JsonTraceSerializer : ITraceSerializer
{
    public string Serialize(ITraceResult traceResult)
    {
        return JsonConvert.SerializeObject(traceResult, Formatting.Indented);
    }
}