﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TracerLib.Models;

[Serializable]
public record ThreadInfo : ITraceResult
{
    [XmlAttribute] public int Id { get; init; }
    private long _elapsed;

    [XmlAttribute]
    public long Elapsed
    {
        get
        {
            _elapsed = 0;
            foreach (var methodInfo in Methods)
            {
                _elapsed += methodInfo.Elapsed;
            }

            return _elapsed;
        }
        init => _elapsed = value;
    }

    [XmlElement(ElementName = "Method")] public List<MethodInfo> Methods { get; init; } = [];

    public ThreadInfo()
    {
    }

    public ThreadInfo(int id)
    {
        Id = id;
    }
}