﻿using TracerLib.Models;

namespace TracerLib.Tracers;

public interface ITracer<out T> where T : ITraceResult
{
    void StartTrace();
    void StopTrace();
    T GetTraceResult();
}