﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TracerLib.Models;

namespace TracerLib.Tracers
{
    public class ThreadTracer : ITracer<ThreadInfo>
    {
        public int Id { get; init; }
        private readonly Stack<MethodTracer> _methodsTracers = new();
        private readonly Stack<MethodInfo> _methodsInfos = new();
        private MethodTracer _currentMethodTracer;

        public void StartTrace()
        {
            //  If there is an existing _currentMethodTracer, it is pushed onto the _methodsTracers stack
            if (_currentMethodTracer != null) _methodsTracers.Push(_currentMethodTracer);
            _currentMethodTracer = new MethodTracer();
            _currentMethodTracer.Stopwatch.Start();
        }

        public void StopTrace()
        {
            if (_currentMethodTracer == null) return;

            // Current MethodTracer instance's stopwatch is stopped
            _currentMethodTracer.Stopwatch.Stop();
            var methodInfo = new MethodInfo
            {
                Name = new StackFrame(2, false).GetMethod()?.Name,
                Class = new StackFrame(2, false).GetMethod()?.ReflectedType?.Name,
                Elapsed = _currentMethodTracer.Stopwatch.ElapsedMilliseconds,
                Methods = _currentMethodTracer.Methods.ToList()
            };

            // If the _methodsTracers stack is empty, it indicates that the method being traced is at the top level,
            // so the method information is added to the _methodsInfos stack
            if (_methodsTracers.Count == 0)
            {
                _methodsInfos.Push(methodInfo);
                _currentMethodTracer = null;
            }
            else
            {
                if (!_methodsTracers.TryPop(out var methodTrace)) return;
                _currentMethodTracer = methodTrace;
                _currentMethodTracer.Methods.Push(methodInfo);
            }
        }

        public ThreadInfo GetTraceResult() => new()
        {
            Id = Id,
            Methods = _methodsInfos.ToList()
        };
    }
}