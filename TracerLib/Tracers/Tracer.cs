﻿using System.Collections.Concurrent;
using System.Linq;
using TracerLib.Models;

namespace TracerLib.Tracers;

public class Tracer : ITracer<TraceResult>
{
    private readonly ConcurrentDictionary<int, ThreadTracer> _threadsTracers = new();

    public void StartTrace()
    {
        var threadId = System.Environment.CurrentManagedThreadId;
        var threadTrace = _threadsTracers.GetOrAdd(threadId, _ => new ThreadTracer { Id = threadId });
        threadTrace.StartTrace();
    }

    public void StopTrace()
    {
        var threadId = System.Environment.CurrentManagedThreadId;
        if (_threadsTracers.TryGetValue(threadId, out var threadTrace))
        {
            threadTrace.StopTrace();
        }
    }

    public TraceResult GetTraceResult()
    {
        return new TraceResult
        {
            ThreadsInfo = _threadsTracers.Values.Select(t => t.GetTraceResult()).ToList()
        };
    }
}