﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TracerLib.Models;

namespace TracerLib.Tracers;

public class MethodTracer : ITracer<MethodInfo>
{
    public Stopwatch Stopwatch { get; } = new();
    public Stack<MethodInfo> Methods { get; } = new();

    public void StartTrace() => Stopwatch.Start();
    public void StopTrace() => Stopwatch.Stop();

    public MethodInfo GetTraceResult() => new()
    {
        Elapsed = Stopwatch.ElapsedMilliseconds,
        Methods = Methods.ToList()
    };
}