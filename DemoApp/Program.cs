﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using TracerLib.Models;
using TracerLib.Serialization;
using TracerLib.Tracers;

namespace DemoApp;

internal abstract class Program
{
    private static void Main()
    {
        var tracer = new Tracer();
        DemoClass1 demoClass1 = new(tracer);
        demoClass1.DemoMethod1();

        var xml = new XmlTraceSerializer().Serialize(tracer.GetTraceResult());
        var json = new JsonTraceSerializer().Serialize(tracer.GetTraceResult());
        Console.WriteLine(json);
        Console.WriteLine(xml);
        File.WriteAllText("json.json", json);
        File.WriteAllText("xml.xml", xml);
    }
}

public class DemoClass1
{
    private readonly ITracer<TraceResult> _tracer;
    private readonly DemoClass2 _demoClass2;

    public DemoClass1(ITracer<TraceResult> tracer)
    {
        _tracer = tracer;
        _demoClass2 = new DemoClass2(_tracer);
    }

    public void DemoMethod1()
    {
        _tracer.StartTrace();

        var events = new List<WaitHandle>();
        var resetEvent = new ManualResetEvent(false);
        ThreadPool.QueueUserWorkItem(_ =>
        {
            _demoClass2.DemoMethod2();
            resetEvent.Set();
        });
        events.Add(resetEvent);

        _demoClass2.DemoMethod2();
        _demoClass2.DemoMethod2();

        WaitHandle.WaitAll(events.ToArray());
        _tracer.StopTrace();
    }
}

public class DemoClass2(ITracer<TraceResult> tracer)
{
    public void DemoMethod2()
    {
        tracer.StartTrace();

        for (var i = 0; i < 2; i++)
        {
            i++;
        }

        tracer.StopTrace();
    }
}