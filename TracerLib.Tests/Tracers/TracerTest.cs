using System.Linq;
using TracerLib.Tracers;
using Xunit;

namespace TracerLib.Tests.Tracers
{
    public class TracerTests
    {
        [Fact]
        public void Tracer_StartStopTrace_AddsThreadTracer()
        {
            // Arrange
            var tracer = new Tracer();

            // Act
            tracer.StartTrace();
            tracer.StopTrace();
            var traceResult = tracer.GetTraceResult();

            // Assert
            Assert.Single(traceResult.ThreadsInfo);
        }

        [Fact]
        public void Tracer_MultipleThreads_AddsMultipleThreadTracers()
        {
            // Arrange
            var tracer = new Tracer();
            const int numThreads = 5;

            // Act
            var threads = new System.Threading.Thread[numThreads];
            for (var i = 0; i < numThreads; i++)
            {
                threads[i] = new System.Threading.Thread(() =>
                {
                    tracer.StartTrace();
                    System.Threading.Thread.Sleep(1); // Simulate method execution time
                    tracer.StopTrace();
                });
            }

            foreach (var thread in threads)
            {
                thread.Start();
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }

            var traceResult = tracer.GetTraceResult();

            // Assert
            Assert.Equal(numThreads, traceResult.ThreadsInfo.Count);
        }

        [Fact]
        public void Tracer_GetTraceResult_ReturnsValidTraceResult()
        {
            // Arrange
            var tracer = new Tracer();

            // Act
            tracer.StartTrace();
            System.Threading.Thread.Sleep(100); // Simulate method execution time
            tracer.StopTrace();
            var traceResult = tracer.GetTraceResult();

            // Assert
            Assert.NotNull(traceResult);
            Assert.NotNull(traceResult.ThreadsInfo);
            Assert.NotEmpty(traceResult.ThreadsInfo);
            Assert.True(traceResult.ThreadsInfo.All(threadInfo => threadInfo != null));
        }
    }
}