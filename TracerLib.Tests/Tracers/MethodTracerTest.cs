using TracerLib.Models;
using TracerLib.Tracers;
using Xunit;

namespace TracerLib.Tests.Tracers
{
    public class MethodTracerTests
    {
        [Fact]
        public void Tracer_StartStopTrace_CalculatesElapsedCorrectly()
        {
            // Arrange
            var tracer = new MethodTracer();

            // Act
            tracer.StartTrace();
            System.Threading.Thread.Sleep(1); // Simulate method execution time
            tracer.StopTrace();
            var traceResult = tracer.GetTraceResult();

            // Assert
            Assert.True(traceResult.Elapsed >= 1, "Elapsed time should be at least 100 milliseconds.");
        }

        [Fact]
        public void Tracer_TraceResultContainsMethodList()
        {
            // Arrange
            var tracer = new MethodTracer();

            // Act
            tracer.Methods.Push(new MethodInfo { Name = "Method1" , Class = "Class1", Elapsed = 1});
            tracer.Methods.Push(new MethodInfo { Name = "Method2" , Class = "Class2", Elapsed = 2});
            var traceResult = tracer.GetTraceResult();

            // Assert for Method1
            var method1 = traceResult.Methods.Find(m => m.Name == "Method1");
            Assert.NotNull(method1);
            Assert.Equal("Class1", method1.Class);
            Assert.Equal(1, method1.Elapsed);

            // Assert for Method2
            var method2 = traceResult.Methods.Find(m => m.Name == "Method2");
            Assert.NotNull(method2);
            Assert.Equal("Class2", method2.Class);
            Assert.Equal(2, method2.Elapsed);
        }
    }
}